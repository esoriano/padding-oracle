/*
Copyright (C) 2019 Enrique Soriano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"crypto/aes"
	"fmt"
	"log"
	"math"
	"net"
	"gitlab.etsit.urjc.es/esoriano/padding-oracle.git/protocol"
)

var (
	chost = "localhost"
	cport = "3333"
	ctype = "tcp"
)

func updateFake(fake []byte, inter []byte, pos int) {
	for i := pos + 1; i < aes.BlockSize; i++ {
		fake[i] = inter[i] ^ byte(aes.BlockSize-pos)
	}
}

func isPaddingOk(payload []byte, conn net.Conn) bool {
	m, err := protocol.NewRawMsg(payload)
	if err != nil {
		log.Fatal(err)
	}
	response, err2 := m.Send(conn)
	if err2 != nil {
		log.Fatal(err2)
	}
	return response != protocol.PadError
}

//prev: previous ciphertext block
//current: current ciphertext block
func decrypt(prev []byte, current []byte, conn net.Conn) []byte {
	fake := make([]byte, aes.BlockSize)  // fake previous cipherblock
	inter := make([]byte, aes.BlockSize) // current intermediate block
	clear := make([]byte, aes.BlockSize) // current plaintext block
	for i := aes.BlockSize - 1; i >= 0; i-- {
		found := false
		updateFake(fake, inter, i)
		for brute := uint8(0); !found; brute++ {
			fake[i] = brute
			payload := append(fake, current...)
			found = isPaddingOk(payload, conn)
			if found {
				inter[i] = brute ^ byte(aes.BlockSize-i)
				clear[i] = prev[i] ^ inter[i]
				log.Printf("plaintext[%d]: 0x%02x\n", i, clear[i])
			} else if brute == math.MaxUint8 {
				log.Fatal("bug")
			}
		}
	}
	return clear
}

func main() {
	tobreak, err := protocol.GetDump("payload.dump")
	clear := make([]byte, 0)
	if err != nil {
		log.Fatal(err)
	}
	conn, err := net.Dial(ctype, chost+":"+cport)
	if err != nil {
		log.Fatal(err)
	}
	prevst := 0
	prevend := aes.BlockSize
	for i := 1; i < len(tobreak)/aes.BlockSize; i++ {
		start := i * aes.BlockSize
		end := (i + 1) * aes.BlockSize
		b := decrypt(tobreak[prevst:prevend], tobreak[start:end], conn)
		log.Printf("block #%d plaintext: %v\n", i, b)
		clear = append(clear, b...)
		prevst = start
		prevend = end
	}
	clearstr, err2 := protocol.UndoPKCS7Padding(clear)
	if err2 != nil {
		log.Fatal(err)
	}
	fmt.Printf("BROKEN PLAINTEXT: %s\n", clearstr)
	conn.Close()
}
