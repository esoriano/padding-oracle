/*
Copyright (C) 2019 Enrique Soriano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"log"
	"net"
	"gitlab.etsit.urjc.es/esoriano/padding-oracle.git/protocol"
)

var (
	chost = "localhost"
	cport = "3333"
	ctype = "tcp"
	key   = []byte("AES256Key-32Characters1234567890")
)

func main() {
	conn, err := net.Dial(ctype, chost+":"+cport)
	if err != nil {
		log.Fatal(err)
	}

	cmd := "CMD: This is a demo of the Padding Oracle Attack implemented in Golang!"
	m, err := protocol.NewMsg(cmd, key)
	if err != nil {
		log.Fatal(err)
	}
	response, err2 := m.Send(conn)
	if err2 != nil {
		log.Fatal(err2)
	}
	log.Printf("Encrypted message sent: %s\n", cmd)
	log.Println(response)
	m.Dump("payload.dump")
	conn.Close()
}
