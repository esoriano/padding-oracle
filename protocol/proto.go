/*
Copyright (C) 2019 Enrique Soriano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"net"
)

const (
	ROK         = iota
	PadError    = iota
	BadCmdError = iota
	otherError  = iota
)

type Msg struct {
	iv      []byte
	payload []byte
}

type Response uint64

func DoPKCS7Padding(buf *[]byte) {
	pad := aes.BlockSize
	r := len(*buf) % aes.BlockSize
	if r != 0 {
		pad = aes.BlockSize - r
	}
	for i := 1; i <= pad; i++ {
		*buf = append(*buf, byte(pad))
	}
}

func UndoPKCS7Padding(buf []byte) (string, error) {
	l := len(buf)
	last := buf[l-1]
	if last < 1 || last > byte(aes.BlockSize) || int(last) > l {
		return "", fmt.Errorf("padding error: last byte is incorrect")
	}
	for i := 1; i <= int(last); i++ {
		if buf[l-i] != last {
			return "", fmt.Errorf("padding error: missing bytes")
		}
	}
	return string(buf[:l-int(last)]), nil
}

func Dump(b []byte) {
	fmt.Printf("###### size = %d ######\n", len(b))
	for i := 0; i < len(b); i++ {
		fmt.Printf("0x%02x ", b[i])
		if i%aes.BlockSize == aes.BlockSize-1 {
			fmt.Println()
		}
	}
	fmt.Printf("#######################\n")
}

func NewMsg(str string, key []byte) (*Msg, error) {
	m := &Msg{}
	m.iv = make([]byte, aes.BlockSize)
	n, err := rand.Read(m.iv)
	if n != aes.BlockSize || err != nil {
		return nil, err
	}
	m.payload = []byte(str)
	DoPKCS7Padding(&m.payload)
	block, err2 := aes.NewCipher(key)
	if err2 != nil {
		return nil, err2
	}
	aes := cipher.NewCBCEncrypter(block, m.iv)
	aes.CryptBlocks(m.payload, m.payload)
	return m, nil
}

func NewRawMsg(payload []byte) (*Msg, error) {
	m := &Msg{}
	m.iv = make([]byte, aes.BlockSize)
	n, err := rand.Read(m.iv)
	if n != aes.BlockSize || err != nil {
		return nil, err
	}
	m.payload = payload
	return m, nil
}

func (m *Msg) Recv(conn net.Conn) error {
	var sz uint64
	m.iv = make([]byte, aes.BlockSize)
	len, err := conn.Read(m.iv)
	if err != nil || len != aes.BlockSize {
		return err
	}
	err = binary.Read(conn, binary.LittleEndian, &sz)
	if err != nil {
		return fmt.Errorf("cannot read size")
	}
	m.payload = make([]byte, sz)
	len, err = conn.Read(m.payload)
	if err != nil {
		return err
	}
	return nil
}

func (m *Msg) Send(conn net.Conn) (Response, error) {
	var r Response
	_, err := conn.Write(m.iv)
	if err != nil {
		return otherError, err
	}
	sz := uint64(len(m.payload))
	err = binary.Write(conn, binary.LittleEndian, &sz)
	if err != nil {
		return otherError, err
	}
	_, err = conn.Write(m.payload)
	if err != nil {
		return otherError, err
	}
	err = binary.Read(conn, binary.LittleEndian, &r)
	if err != nil {
		return otherError, fmt.Errorf("cannot read response")
	}
	switch Response(r) {
	case ROK, PadError, BadCmdError:
		return Response(r), nil
	}
	return otherError, fmt.Errorf("not a valid response")
}

func (m *Msg) Decrypt(key []byte) (string, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	aes := cipher.NewCBCDecrypter(block, m.iv)
	aes.CryptBlocks(m.payload, m.payload)
	str, err2 := UndoPKCS7Padding(m.payload)
	if err2 != nil {
		return "", err2
	}
	return str, nil
}

func (m *Msg) Dump(path string) error {
	all := append(m.iv, m.payload...)
	return ioutil.WriteFile(path, all, 0644)
}

// wrapping like a champ :)
func GetDump(path string) ([]byte, error) {
	return ioutil.ReadFile(path)
}

func respond(conn net.Conn, r Response) error {
	switch r {
	case ROK, PadError, BadCmdError:
		return binary.Write(conn, binary.LittleEndian, &r)
	default:
		return fmt.Errorf("not a valid response")
	}
}

func PaddingError(c net.Conn) error {
	return respond(c, PadError)
}

func InvalidCmdError(c net.Conn) error {
	return respond(c, BadCmdError)
}

func RespOK(c net.Conn) error {
	return respond(c, ROK)
}

func (r Response) String() string {
	switch r {
	case ROK:
		return "OK"
	case PadError:
		return "Padding Error"
	case BadCmdError:
		return "Invalid Command"
	default:
		return "Other error"
	}
}
