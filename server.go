/*
Copyright (C) 2019 Enrique Soriano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"gitlab.etsit.urjc.es/esoriano/padding-oracle.git/protocol"
	"strings"
)

var (
	chost = "localhost"
	cport = "3333"
	ctype = "tcp"
	key   = []byte("AES256Key-32Characters1234567890")
)

// Requests (cleartext) are strings starting by "CMD: "
// Responses are  integers: ok, paddingerror or invalidcmd
func handleConn(conn net.Conn) {
	var err error
	msg := &protocol.Msg{}
	for {
		if err = msg.Recv(conn); err != nil {
			if err == io.EOF {
				log.Println("Connection closed")
			} else {
				log.Printf("Closing connection: %s\n", err)
			}
			conn.Close()
			return
		}
		if cmd, err2 := msg.Decrypt(key); err2 != nil {
			log.Println("Padding error")
			protocol.PaddingError(conn)
		} else if !strings.HasPrefix(cmd, "CMD: ") {
			log.Println("Invalid command")
			protocol.InvalidCmdError(conn)
		} else {
			log.Println("Processing command:  " + cmd)
			protocol.RespOK(conn)
		}
	}
}

func main() {
	l, err := net.Listen(ctype, chost+":"+cport)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	defer l.Close()
	fmt.Println("Listening on " + chost + ":" + cport)
	for {
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		go handleConn(conn)
	}
}
